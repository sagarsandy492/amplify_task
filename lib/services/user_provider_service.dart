import 'package:flutter/material.dart';

class UserProviderService extends ChangeNotifier {
  String email;
  String gender;
  String trainingPeriod;

  storeUserData(emailValue) {
    email = emailValue;
    notifyListeners();
  }

  storeGenderValue(genderValue) {
    gender = genderValue;
    notifyListeners();
  }
}
