import 'package:flutter/material.dart';

class RegisterPageIndicatorWidget extends StatelessWidget {
  final double thickness;

  RegisterPageIndicatorWidget({this.thickness});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2 - 24,
      child: Divider(
        height: 10,
        thickness: thickness,
        color: Color(0xFFf2d493),
      ),
    );
  }
}
