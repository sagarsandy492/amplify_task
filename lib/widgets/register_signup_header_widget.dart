import 'package:amplify_task/widgets/register_page_indicator_widget.dart';
import 'package:flutter/material.dart';

class RegisterSignUpHeaderWidget extends StatelessWidget {
  final int stepNumber;
  RegisterSignUpHeaderWidget({this.stepNumber});
  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Container(
          color: Colors.black,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "SIGN UP",
                style: TextStyle(
                  color: Color(0xFFf2d493),
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RegisterPageIndicatorWidget(thickness: 4),
                  RegisterPageIndicatorWidget(
                      thickness: stepNumber == 2 ? 4 : 1),
                ],
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
