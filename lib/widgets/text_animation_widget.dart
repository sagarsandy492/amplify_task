import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class TextAnimationWidget extends StatelessWidget {
  final Function onFinishedCallback;

  TextAnimationWidget({this.onFinishedCallback});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 25.0,
      child: FadeAnimatedTextKit(
        duration: Duration(seconds: 1),
        isRepeatingAnimation: false,
        text: ["5", "4", "3", "2", "1"],
        textStyle: TextStyle(
            fontSize: 32.0,
            fontFamily: "Poppins",
            fontWeight: FontWeight.bold,
            color: Colors.white),
        textAlign: TextAlign.start,
        onFinished: onFinishedCallback,
      ),
    );
  }
}
