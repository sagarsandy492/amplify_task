import 'package:flutter/material.dart';

class TextFieldWidget extends StatelessWidget {
  final TextEditingController textEditingController;
  final String hintText;
  final bool isSecureData;
  final Function validateFieldValue;
  final bool isFilled;
  final Color textColor;

  TextFieldWidget({
    this.textEditingController,
    this.hintText,
    this.isSecureData,
    this.validateFieldValue,
    this.isFilled,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textEditingController,
      obscureText: isSecureData,
      style: TextStyle(
        fontFamily: "Poppins",
        color: textColor,
        fontSize: 14,
      ),
      decoration: InputDecoration(
        filled: isFilled,
        fillColor: Color(0XFFF5F5F5),
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0XFFE2E2E2), width: 1.5),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0XFFE2E2E2), width: 1.5),
        ),
        errorBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red, width: 1.5),
        ),
        focusedErrorBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.red, width: 1.5),
        ),
        hintText: hintText,
        errorStyle: TextStyle(
          fontFamily: "Poppins",
          color: Colors.red,
          fontSize: 13,
        ),
        contentPadding:
            new EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
        hintStyle: TextStyle(
          fontFamily: "Poppins",
          color: Colors.grey,
          fontSize: 14,
        ),
      ),
      validator: validateFieldValue,
    );
  }
}
