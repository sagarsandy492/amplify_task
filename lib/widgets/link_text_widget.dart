import 'package:flutter/material.dart';

class LinkTextWidget extends StatelessWidget {
  final String title;
  final Function gestureCallBack;
  LinkTextWidget({this.title, this.gestureCallBack});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gestureCallBack,
      child: Text(
        title,
        style: TextStyle(
            fontFamily: "Poppins",
            color: Color(0xFFf2d493),
            fontSize: 13,
            fontWeight: FontWeight.w600),
      ),
    );
  }
}
