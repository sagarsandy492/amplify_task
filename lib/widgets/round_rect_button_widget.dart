import 'package:flutter/material.dart';

class RoundRectButtonWidget extends StatelessWidget {
  final String title;
  final Function onPressCallBack;
  RoundRectButtonWidget({this.title, this.onPressCallBack});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressCallBack,
      child: Container(
        height: 42,
        width: 325,
        decoration: BoxDecoration(
          color: Color(0xFFf2d493),
          borderRadius: BorderRadius.circular(25),
        ),
        child: Center(
          child: Text(
            title,
            style: TextStyle(
              fontFamily: "Poppins",
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
