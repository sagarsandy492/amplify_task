import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Transform.rotate(
          angle: 22,
          child: const Text(
            'A',
            style: TextStyle(
              fontFamily: "Poppins",
              fontSize: 50,
              fontStyle: FontStyle.italic,
              color: Color(0xFFf2d493),
            ),
          ),
        ),
        Text(
          "mplify",
          style: TextStyle(
            fontFamily: "Poppins",
            fontStyle: FontStyle.italic,
            fontSize: 50,
            color: Color(0xFFf2d493),
          ),
        ),
      ],
    );
  }
}
