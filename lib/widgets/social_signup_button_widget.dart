import 'package:flutter/material.dart';

class SocialSignUpButtonWidget extends StatelessWidget {
  final String imagePath;
  final String buttonName;
  final Color textColor;
  final Color backGroundColor;
  final Function gestureCallBack;

  SocialSignUpButtonWidget(
      {this.imagePath,
      this.buttonName,
      this.textColor,
      this.backGroundColor,
      this.gestureCallBack});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gestureCallBack,
      child: Container(
        height: 42,
        width: 325,
        decoration: BoxDecoration(
          color: backGroundColor,
          borderRadius: BorderRadius.circular(25),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imagePath,
              width: 23,
              height: 23,
            ),
            Text(
              buttonName,
              style: TextStyle(
                color: textColor,
                fontSize: 15,
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
