import 'package:amplify_task/widgets/plain_text_widget.dart';
import 'package:flutter/material.dart';

class SocialLogInButtonWidget extends StatelessWidget {
  final String imageIconPath;
  final String buttonTitle;
  final Function gestureCallback;
  SocialLogInButtonWidget(
      {this.imageIconPath, this.buttonTitle, this.gestureCallback});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: gestureCallback,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            imageIconPath,
            width: 23,
            height: 23,
          ),
          PlainTextWidget(title: buttonTitle),
        ],
      ),
    );
  }
}
