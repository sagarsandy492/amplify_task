import 'package:flutter/material.dart';

class SemiBoldTextWidget extends StatelessWidget {
  final String title;
  final FontWeight fontWeight;
  SemiBoldTextWidget({this.title, this.fontWeight});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: "Poppins",
        fontWeight: fontWeight,
        color: Colors.black87,
        fontSize: 13,
      ),
    );
  }
}
