import 'package:flutter/material.dart';

class PlainTextWidget extends StatelessWidget {
  final String title;
  PlainTextWidget({this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: "Poppins",
        color: Colors.white,
        fontSize: 13,
      ),
    );
  }
}
