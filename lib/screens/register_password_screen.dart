import 'package:amplify_task/screens/welcome_screen.dart';
import 'package:amplify_task/widgets/register_signup_header_widget.dart';
import 'package:amplify_task/widgets/round_rect_button_widget.dart';
import 'package:amplify_task/widgets/semi_bold_text_widget.dart';
import 'package:amplify_task/widgets/textfield_widget.dart';
import 'package:flutter/material.dart';

class RegisterPasswordScreen extends StatefulWidget {
  @override
  _RegisterPasswordScreenState createState() => _RegisterPasswordScreenState();
}

class _RegisterPasswordScreenState extends State<RegisterPasswordScreen> {
  final TextEditingController passwordTEC = TextEditingController();
  final TextEditingController confirmPasswordTEC = TextEditingController();
  String password, confirmPassword;
  final formKey = GlobalKey<FormState>(); // Used for register form

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          // Rendering header widget
          RegisterSignUpHeaderWidget(stepNumber: 2),
          Flexible(
            flex: 12,
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Form(
                  key: formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SemiBoldTextWidget(
                          title: "Choose a password",
                          fontWeight: FontWeight.w600),
                      SizedBox(height: 20),
                      // Rendering password text field widget
                      TextFieldWidget(
                        textEditingController: passwordTEC,
                        hintText: "Password",
                        isSecureData: true,
                        isFilled: true,
                        textColor: Colors.black,
                        validateFieldValue: (String passwordValue) {
                          if (passwordValue.length < 8) {
                            return "Minimum of 8 characters";
                          }
                          if (!passwordValue.contains(new RegExp(r'[A-Z]'))) {
                            return "Must have a capital letter";
                          }
                          if (!passwordValue.contains(new RegExp(r'[a-z]'))) {
                            return "Must have a lowercase letter";
                          }
                          if (!passwordValue.contains(new RegExp(r'[0-9]'))) {
                            return "Should contain a number";
                          }
                        },
                      ),
                      SizedBox(height: 20),
                      // Rendering confirm password text field widget
                      TextFieldWidget(
                        textEditingController: confirmPasswordTEC,
                        hintText: "Confirm password",
                        isSecureData: true,
                        isFilled: true,
                        textColor: Colors.black,
                        validateFieldValue: (String passwordValue) {
                          if (passwordTEC.text != passwordValue) {
                            return "Both passwords should match";
                          }
                        },
                      ),
                      // Rendering continue button widget
                      Expanded(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: RoundRectButtonWidget(
                            title: "CONTINUE",
                            onPressCallBack: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              if (formKey.currentState.validate()) {
                                print("success");
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => WelcomeScreen(),
                                  ),
                                );
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
