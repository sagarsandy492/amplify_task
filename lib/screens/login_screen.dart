import 'package:amplify_task/screens/signup_screen.dart';
import 'package:amplify_task/screens/welcome_screen.dart';
import 'package:amplify_task/services/user_provider_service.dart';
import 'package:amplify_task/widgets/link_text_widget.dart';
import 'package:amplify_task/widgets/logo_widget.dart';
import 'package:amplify_task/widgets/plain_text_widget.dart';
import 'package:amplify_task/widgets/round_rect_button_widget.dart';
import 'package:amplify_task/widgets/social_login_button_widget.dart';
import 'package:amplify_task/widgets/text_animation_widget.dart';
import 'package:amplify_task/widgets/textfield_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController userNameTEC = TextEditingController();
  final TextEditingController passwordTEC = TextEditingController();
  final formKey = GlobalKey<FormState>(); // Used for login form
  bool showTimer = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        leading: Icon(Icons.arrow_back_ios),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // Rendering Logo widget
            LogoWidget(),
            Form(
              key: formKey,
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Column(
                  children: [
                    // Rendering email text field widget
                    TextFieldWidget(
                      textEditingController: userNameTEC,
                      hintText: "Username or Email address",
                      isSecureData: false,
                      isFilled: false,
                      textColor: Colors.white,
                      validateFieldValue: (String userNameFieldValue) {
                        if (!userNameFieldValue.isValidEmail()) {
                          return "Wrong email format";
                        }
                      },
                    ),
                    SizedBox(height: 10),
                    // Rendering password text field widget
                    TextFieldWidget(
                      textEditingController: passwordTEC,
                      hintText: "Password",
                      isSecureData: true,
                      isFilled: false,
                      textColor: Colors.white,
                      validateFieldValue: (String passwordFieldValue) {
                        if (passwordFieldValue.length < 3) {
                          return "Password should be more than 3 characters";
                        }
                      },
                    ),
                    SizedBox(height: 15),
                    // Rendering forgot password text widget
                    Align(
                      alignment: Alignment.bottomRight,
                      child: LinkTextWidget(
                        title: "Forgotten password?",
                        gestureCallBack: () {},
                      ),
                    ),
                    SizedBox(height: 15),
                    // Rendering login button widget
                    RoundRectButtonWidget(
                      title: "LOG IN",
                      onPressCallBack: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        if (formKey.currentState.validate()) {
                          print("success");
                          Provider.of<UserProviderService>(context,
                                  listen: false)
                              .storeUserData(userNameTEC.text);
                          setState(() {
                            showTimer = true;
                          });
                        }
                      },
                    ),
                    SizedBox(height: 20),
                    // Rendering text animation widget after login button press event
                    if (showTimer)
                      TextAnimationWidget(
                        onFinishedCallback: () {
                          showTimer = false;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => WelcomeScreen(),
                            ),
                          );
                        },
                      )
                    else
                      SizedBox(height: 10),
                    // Rendering facebook button widget
                    SocialLogInButtonWidget(
                      imageIconPath: "assets/images/facebook.png",
                      buttonTitle: "   Continue with Facebook",
                      gestureCallback: () =>
                          _launchURL("https://www.facebook.com"),
                    ),
                    SizedBox(height: 20),
                    // Rendering google button widget
                    SocialLogInButtonWidget(
                      imageIconPath: "assets/images/google.png",
                      buttonTitle: "   Continue with Google",
                      gestureCallback: () =>
                          _launchURL("https://www.gmail.com"),
                    ),
                    SizedBox(height: 30),
                    Divider(
                      height: 10,
                      thickness: 1,
                      color: Colors.white54,
                    ),
                    SizedBox(height: 20),
                    // Rendering sign up button widget
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        PlainTextWidget(title: "Don't have an account?"),
                        LinkTextWidget(
                          title: " Sign up",
                          gestureCallBack: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SignUpScreen(),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Opening url in browser
  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

// Email validator extension
extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
