import 'package:amplify_task/screens/register_screen.dart';
import 'package:amplify_task/widgets/link_text_widget.dart';
import 'package:amplify_task/widgets/logo_widget.dart';
import 'package:amplify_task/widgets/plain_text_widget.dart';
import 'package:amplify_task/widgets/social_signup_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(25.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // Rendering logo widget
              LogoWidget(),
              Container(
                width: 200,
                height: 90,
                // color: Colors.cyan,
                child: Center(
                  child: Text(
                    "GAIN BACK CONTROL OF YOUR LIFE",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              // Rendering facebook button widget
              SocialSignUpButtonWidget(
                imagePath: "assets/images/facebook.png",
                buttonName: "   SIGN UP WITH FACEBOOK",
                textColor: Colors.white,
                backGroundColor: Color(0xFF256ef0),
                gestureCallBack: () => _launchURL("https://www.facebook.com"),
              ),
              SizedBox(height: 20),
              // Rendering google button widget
              SocialSignUpButtonWidget(
                imagePath: "assets/images/google.png",
                buttonName: "   SIGN UP WITH GOOGLE",
                textColor: Colors.black,
                backGroundColor: Colors.white,
                gestureCallBack: () => _launchURL("https://www.gmail.com"),
              ),
              SizedBox(height: 30),
              // Rendering email/password sign up button widget
              LinkTextWidget(
                title: "Sign up with email address",
                gestureCallBack: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => RegisterScreen(),
                    ),
                  );
                },
              ),
              SizedBox(height: 50),
              Divider(
                height: 10,
                thickness: 1,
                color: Colors.white54,
              ),
              SizedBox(height: 30),
              // Rendering sign in button widget
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  PlainTextWidget(title: "Already have an account?"),
                  LinkTextWidget(
                    title: " Sign in",
                    gestureCallBack: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  // Opening url in browser
  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
