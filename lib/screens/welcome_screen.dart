import 'package:amplify_task/services/user_provider_service.dart';
import 'package:amplify_task/widgets/plain_text_widget.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: Consumer<UserProviderService>(
          builder: (context, userNotifyProvider, _) {
        return Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              PlainTextWidget(title: userNotifyProvider.email),
              // Rendering welcome text animation
              SizedBox(
                width: 250.0,
                child: TyperAnimatedTextKit(
                  speed: Duration(seconds: 1),
                  isRepeatingAnimation: false,
                  text: ["Welcome", "to", "the", "Amplify", "fitness"],
                  textStyle: TextStyle(
                      fontFamily: "Poppins",
                      fontSize: 32.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
