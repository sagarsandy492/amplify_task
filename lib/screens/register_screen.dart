import 'dart:async';

import 'package:amplify_task/screens/register_password_screen.dart';
import 'package:amplify_task/services/user_provider_service.dart';
import 'package:amplify_task/widgets/register_signup_header_widget.dart';
import 'package:amplify_task/widgets/round_rect_button_widget.dart';
import 'package:amplify_task/widgets/semi_bold_text_widget.dart';
import 'package:amplify_task/widgets/textfield_widget.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController emailTEC = TextEditingController();
  String email;
  final formKey = GlobalKey<FormState>(); // Used for register form

  // Dropdown items
  List<ListItem> _dropdownItems = [
    ListItem(1, "Once a day"),
    ListItem(2, "Twice in a day"),
    ListItem(3, "Once in a week"),
    ListItem(4, "Twice in a week"),
    ListItem(5, "Thrice in a week"),
  ];

  List<DropdownMenuItem<ListItem>> _dropdownMenuItems;
  ListItem _selectedItem;

  void initState() {
    super.initState();
    Timer(Duration(milliseconds: 5), () {
      manageGenderStatus("male");
    });
    _dropdownMenuItems = buildDropDownMenuItems(_dropdownItems);
    _selectedItem = _dropdownMenuItems[0].value;
  }

  // Dropdown menu items
  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(
            listItem.name,
            style: TextStyle(fontFamily: "Poppins", fontSize: 13),
          ),
          value: listItem,
        ),
      );
    }
    return items;
  }

  // Manage gender toggle status
  manageGenderStatus(String genderValue) {
    Provider.of<UserProviderService>(context, listen: false)
        .storeGenderValue(genderValue);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
      ),
      body: Column(
        children: [
          // Rendering sg=ignup widget
          RegisterSignUpHeaderWidget(stepNumber: 1),
          Flexible(
            flex: 12,
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: Form(
                key: formKey,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SemiBoldTextWidget(
                          title: "Whats your email address?",
                          fontWeight: FontWeight.w600),
                      SizedBox(height: 15),
                      // Rendering email text field widget
                      TextFieldWidget(
                        textEditingController: emailTEC,
                        hintText: "Email",
                        isSecureData: false,
                        isFilled: true,
                        textColor: Colors.black,
                        validateFieldValue: (String emailFieldValue) {
                          if (!emailFieldValue.isValidEmail()) {
                            return "Wrong email format";
                          }
                        },
                      ),
                      SizedBox(height: 30),
                      // Rendering gender checkbox widget
                      SemiBoldTextWidget(
                          title: "Whats your gender?",
                          fontWeight: FontWeight.w600),
                      Consumer<UserProviderService>(
                          builder: (context, userNotifyProvider, _) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SemiBoldTextWidget(
                                title: "Male", fontWeight: FontWeight.w300),
                            CircularCheckBox(
                              value: userNotifyProvider.gender == "male"
                                  ? true
                                  : false,
                              onChanged: (changed) {
                                if (changed) {
                                  manageGenderStatus("male");
                                }
                              },
                              activeColor: Color(0XFF5BC76C),
                              checkColor: Colors.black,
                            ),
                            SemiBoldTextWidget(
                                title: "Female", fontWeight: FontWeight.w300),
                            CircularCheckBox(
                              value: userNotifyProvider.gender == "female"
                                  ? true
                                  : false,
                              onChanged: (changed) {
                                if (changed) {
                                  manageGenderStatus("female");
                                }
                              },
                              activeColor: Color(0XFF5BC76C),
                              checkColor: Colors.black,
                            ),
                            SemiBoldTextWidget(
                                title: "N/A", fontWeight: FontWeight.w300),
                            CircularCheckBox(
                              value: userNotifyProvider.gender == "na"
                                  ? true
                                  : false,
                              onChanged: (changed) {
                                if (changed) {
                                  manageGenderStatus("na");
                                }
                              },
                              activeColor: Color(0XFF5BC76C),
                              checkColor: Colors.black,
                            ),
                          ],
                        );
                      }),
                      SizedBox(height: 20),
                      SemiBoldTextWidget(
                        title: "How much do you train?",
                        fontWeight: FontWeight.w600,
                      ),
                      // Rendering dropdown widget
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding:
                              const EdgeInsets.only(left: 10.0, right: 10.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              color: Color(0XFFF5F5F5),
                              border: Border.all(
                                color: Color(0XFFE2E2E2),
                              )),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              icon: Icon(
                                Icons.keyboard_arrow_down_outlined,
                                size: 30,
                              ),
                              value: _selectedItem,
                              items: _dropdownMenuItems,
                              onChanged: (value) {
                                setState(() {
                                  _selectedItem = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      // Rendering next button widget
                      Expanded(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: RoundRectButtonWidget(
                            title: "NEXT",
                            onPressCallBack: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              if (formKey.currentState.validate()) {
                                manageUserRegistrationData();
                                print("success");
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Store user data and navigate to password screen
  manageUserRegistrationData() {
    Provider.of<UserProviderService>(context, listen: false)
        .storeUserData(emailTEC.text);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RegisterPasswordScreen(),
      ),
    );
  }
}

// Email validator extension
extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}
